package main

import (
	"encoding/json"
	"fmt"
)

//JSON type
type JSON string

//type JSON struct{}

//Convert type implementing Convertfile interface...should write to destination file or endpoint
func (j JSON) Convert(hotels []Hotel) ([]byte, error) {
	hotelJSON, err := json.Marshal(hotels)
	if err != nil {
		fmt.Printf("Could not convert to JSON format. following error: %v \n", err.Error())
	}
	return hotelJSON, nil
}

//Type ...
func (j JSON) Type() string {
	return "json"
}
