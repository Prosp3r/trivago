package main

import (
	"fmt"
	"net/url"
	"strconv"
	"unicode/utf8"
)

//:::::::::::::::::::VALIDATION CODE::::::::::::::::::::::::::::::::
//isValid ...this should be extendable in its own module and folders

//a. A hotel name may only contain UTF-8 characters.
//b. The hotel URL must be valid (please come up with a good definition of "valid").
//c. Hotel ratings are given as a number from 0 to 5 stars (no negative numbers).

//IsValidHotel will test all hotel parameters for set validity
func IsValidHotel(line []string) bool {
	//name validation
	//fmt.Printf("Reading line: %v \n", line)
	//fmt.Printf("Name: %v -- Address: %v -- Stars: %v -- Contact: %v -- Phone: %v -- URI: %v \n", line[0], line[1], line[2], line[3], line[4], line[5])
	if len(line[0]) < 2 || IsValidUTF(line[0]) == false {
		return false
	}
	//validate address
	rating, err := strconv.Atoi(line[2])
	if err != nil {
		fmt.Println("Error converting to rating to int")
	}
	//fmt.Printf("Rating : %v \n", rating)
	if rating < int(0) || rating > int(5) {
		//InvalidEntry = +1
		return false
	}

	//Validate uri
	if validateURI(line[5]) == false {
		fmt.Printf("Invalid URI : %v \n", line[5])
		return false
		//os.Exit(1)
	}
	//ValidEntry = +1
	return true
}

//validateURI checks for url schemes, host, path...does not verify actual availability
func validateURI(uri string) bool {
	//u, err := url.Parse("http:::/not.valid/a//a??a?b=&&c#hi")
	u, err := url.Parse(uri)
	if err != nil {
		return false
		//log.Fatal(err)
	}
	//structure verification
	if u.Scheme == "" || u.Host == "" || u.Path == "" {
		return false
		//log.Fatal("invalid URL")
	}
	return true
}

//IsValidUTF will return true if string is a valid UTF8
func IsValidUTF(s string) bool {

	beging := s
	//fmt.Printf("%q\n", beging)
	if !utf8.ValidString(s) {
		v := make([]rune, 0, len(s))
		for i, r := range s {
			if r == utf8.RuneError {
				_, size := utf8.DecodeRuneInString(s[i:])
				if size == 1 {
					continue
				}
			}
			v = append(v, r)
		}
		s = string(v)
	}

	finished := s
	//fmt.Printf("%q\n", finished)
	if len(beging) == len(finished) {
		return true
	}
	return false
}
