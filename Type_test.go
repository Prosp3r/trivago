package main

import "testing"

func TestType(t *testing.T) {
	var yamlFormats YAML
	var jsonFormats JSON
	yamlExt := string(yamlFormats.Type())
	jsonExt := string(jsonFormats.Type())

	if yamlExt != "yml" {
		t.Fatalf("Failed YAML extension Type test returned with wrong result %s \n", failed)
	} else {
		t.Logf("Success YAML extension Type returned with expected result %s \n", succeed)
	}

	if jsonExt != "json" {
		t.Fatalf("Failed JSON extension Type test returned with wrong result %s \n", failed)
	} else {
		t.Logf("Success JSON extension Type returned with expected result %s \n", succeed)
	}
}
