package main 

import(
	"fmt"
	"github.com/go-yaml/yaml"
)

//YAML type
type YAML string

//type YAML struct{}

//Convert yaml type implementation of Convertfile interface write to destination file or endpoint
func (y YAML) Convert(hotels []Hotel) ([]byte, error) {
	hotelYAML, err := yaml.Marshal(hotels)
	if err != nil {
		fmt.Printf("Could not convert to YAML format. following error: %v \n", err.Error())
	}
	return hotelYAML, nil
}

//Type implements convertfile interface type method returns the extension of the format type
func (y YAML) Type() string {
	return "yml"
}
