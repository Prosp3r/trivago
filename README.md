## TRIVAFORM - A Simple Command Line Utility For Data Conversion

# How the user should experience this tool
1. User installs trivaform with two commands maximum
2. User calls the commandline sepcifying the location of file to be converted and destination
4. User finds files in new format in destination folder.


# Assumptions
- Data source is only in .csv format

# Features
 - Extensible to other formats with very few lines (sticking to standard library implementations as much as possible)
 - Exposes a commandline interface for reads and output flags
 - Sort data output by name and by ratings
 - Supply alternative data source using the src flag
 - Optionaly display operation statistics using the stats flag.

# How to Installation
1. Extract the zip file to a linux box
2. Will create a docker alternative at free time or on demand.


# How to use
1. On linux terminal (command line) run ./trivago
2. Optionaly you can run the command with flags for more fun. 
    FLAGS
    =====
    
``` 
    h	    Display this usage guide
    src     Source file location to be converted e.g. newfile.csv
    ordern	Sort/Order alphabeticaly by name when writing to file
    orderr	Sort/Order by rating score when writing to file
    stats	Display stats at the end of operation
    v	    Display version number
```

E.g:
` ./trivago -h`
` ./trivago -v `
` ./trivago -ordern -stats `
` ./trivago -orderr -stats `
` ./trivago -src=newfile.csv `


# How to extend output format
1. Implement the Convertfile interface in a new file (named newFormat.go) as shown in json.go or yaml.go files.
2. Add your format to the list below line 49 on main.go
3. Add/mimic/call relvant functions on lines: 92, 103, 108 and 113
4. Write test for your extension (NOTE: Test Coverage failes at 50% coverage so please surpass that)
5. run go build to produce a new binary

 

# Design Decision
1. Every part that's extensible have been separated into a file. 
(Ideay these could be a module depending on how large)

   - Sort could be improved in many way so it's kept in a separate file
   - File formats could be extended so they're kept each new format in it's own file
   - Validation of all kinds are kept in a single file
   - Flags are kept in a file of its own

