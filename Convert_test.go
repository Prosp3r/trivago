package main

import (
	"fmt"
	"testing"
)

/*const (
	succeed = "\u2713"
	failed  = "\u2717"
)*/

func TestJSONConvert(t *testing.T) {

	/*
		hotels := []struct {
			Name    string
			Address string
			Stars   int
			Contact string
			Phone   string
			URI     string
		}
	*/
	hotels := []Hotel{
		{"The Gibson", "63847 Lowe Knoll, East Maxine, WA 97030-4876", 5, "Dr. Sinda Wyman", "1-270-665-9933x1626", "http://www.paucek.com/search.htm"},
		//{"Apartment Dörr Bolzmannweg", "451, 05116 Hannover", 1, "Scarlet Kusch-Linke", "08177354570", "http://www.garden.com/list/home.html"},
	}
	expectedJResult := []byte{91, 123, 34, 78, 97, 109, 101, 34, 58, 34, 84, 104, 101, 32, 71, 105, 98, 115, 111, 110, 34, 44, 34, 65, 100, 100, 114, 101, 115, 115, 34, 58, 34, 54, 51, 56, 52, 55, 32, 76, 111, 119, 101, 32, 75, 110, 111, 108, 108, 44, 32, 69, 97, 115, 116, 32, 77, 97, 120, 105, 110, 101, 44, 32, 87, 65, 32, 57, 55, 48, 51, 48, 45, 52, 56, 55, 54, 34, 44, 34, 83, 116, 97, 114, 115, 34, 58, 53, 44, 34, 67, 111, 110, 116, 97, 99, 116, 34, 58, 34, 68, 114, 46, 32, 83, 105, 110, 100, 97, 32, 87, 121, 109, 97, 110, 34, 44, 34, 80, 104, 111, 110, 101, 34, 58, 34, 49, 45, 50, 55, 48, 45, 54, 54, 53, 45, 57, 57, 51, 51, 120, 49, 54, 50, 54, 34, 44, 34, 85, 82, 73, 34, 58, 34, 104, 116, 116, 112, 58, 47, 47, 119, 119, 119, 46, 112, 97, 117, 99, 101, 107, 46, 99, 111, 109, 47, 115, 101, 97, 114, 99, 104, 46, 104, 116, 109, 34, 125, 93}
	expectedYResult := []byte{45, 32, 110, 97, 109, 101, 58, 32, 84, 104, 101, 32, 71, 105, 98, 115, 111, 110, 10, 32, 32, 97, 100, 100, 114, 101, 115, 115, 58, 32, 54, 51, 56, 52, 55, 32, 76, 111, 119, 101, 32, 75, 110, 111, 108, 108, 44, 32, 69, 97, 115, 116, 32, 77, 97, 120, 105, 110, 101, 44, 32, 87, 65, 32, 57, 55, 48, 51, 48, 45, 52, 56, 55, 54, 10, 32, 32, 115, 116, 97, 114, 115, 58, 32, 53, 10, 32, 32, 99, 111, 110, 116, 97, 99, 116, 58, 32, 68, 114, 46, 32, 83, 105, 110, 100, 97, 32, 87, 121, 109, 97, 110, 10, 32, 32, 112, 104, 111, 110, 101, 58, 32, 49, 45, 50, 55, 48, 45, 54, 54, 53, 45, 57, 57, 51, 51, 120, 49, 54, 50, 54, 10, 32, 32, 117, 114, 105, 58, 32, 104, 116, 116, 112, 58, 47, 47, 119, 119, 119, 46, 112, 97, 117, 99, 101, 107, 46, 99, 111, 109, 47, 115, 101, 97, 114, 99, 104, 46, 104, 116, 109, 10}
	//for ht, i := range hotels {
	jasonOutput, err := jsonFormats.Convert(hotels)
	if err != nil {
		fmt.Printf("An error was encountered: %v \n", err.Error())
	}
	//fmt.Println(jasonOutput)
	//fmt.Println(string(jasonOutput))
	//fmt.Println(string(expectecResult))
	yamlOutput, err := yamlFormats.Convert(hotels)
	if err != nil {
		fmt.Printf("An error was encountered: %v \n", err.Error())
	}
	//fmt.Println(yamlOutput)

	if string(jasonOutput) != string(expectedJResult) {
		t.Fatalf("Failed JSON.Convert returned with inappropriate result %s \n", failed)
	} else {
		t.Logf("Success JSON.Convert returned with expected result %s \n", succeed)
	}

	if string(yamlOutput) != string(expectedYResult) {
		t.Fatalf("Failed YAML.Convert returned with inappropriate result %s \n", failed)
	} else {
		t.Logf("Success YAML.Convert returned with expected result %s \n", succeed)
	}

	//}

}
