package main

import (
	"sort"
)

//SortByName ...
func SortByName(hotel []Hotel) []Hotel {
	// Sort by name
	//fmt.Println("By name:", hotels)
	sort.SliceStable(hotel, func(i, j int) bool { return hotel[i].Name < hotel[j].Name })
	//fmt.Println("By name:", hotels)
	return hotel
}

//SortByRating ..
func SortByRating(hotel []Hotel) []Hotel {
	// Sort by age preserving name order
	sort.SliceStable(hotel, func(i, j int) bool { return hotel[i].Stars < hotel[j].Stars })
	//fmt.Println("By age,name:", hotels)
	return hotel
}
