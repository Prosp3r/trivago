package main

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestSorthotel(t *testing.T) {

	hotelx := []Hotel{
		{"One Gibson", "63847 Lowe Knoll, East Maxine, WA 97030-4876", 5, "Dr. Sinda Wyman", "1-270-665-9933x1626", "http://www.paucek.com/search.htm"},
		{"Apartment Dörr Bolzmannweg", "451, 05116 Hannover", 1, "Scarlet Kusch-Linke", "08177354570", "http://www.garden.com/list/home.html"},
		{"The Gibson", "63847 Lowe Knoll, East Maxine, WA 97030-4876", 4, "Dr. Sinda Wyman", "1-270-665-9933x1626", "http://www.paucek.com/search.htm"},
		{"Bpartment Dörr Bolzmannweg", "451, 05116 Hannover", 2, "Scarlet Kusch-Linke", "08177354570", "http://www.garden.com/list/home.html"},
	}

	//TEST SORT BY NAME
	sortName, err := json.Marshal(SortByName(hotelx))
	//_, err := json.Marshal(SortByName(hotelx))
	if err != nil {
		fmt.Println(err.Error())
	}
	expectedNameR, err := json.Marshal([]Hotel{
		//_, err = json.Marshal([]Hotel{
		{"Apartment Dörr Bolzmannweg", "451, 05116 Hannover", 1, "Scarlet Kusch-Linke", "08177354570", "http://www.garden.com/list/home.html"},
		{"Bpartment Dörr Bolzmannweg", "451, 05116 Hannover", 2, "Scarlet Kusch-Linke", "08177354570", "http://www.garden.com/list/home.html"},
		{"One Gibson", "63847 Lowe Knoll, East Maxine, WA 97030-4876", 5, "Dr. Sinda Wyman", "1-270-665-9933x1626", "http://www.paucek.com/search.htm"},
		{"The Gibson", "63847 Lowe Knoll, East Maxine, WA 97030-4876", 4, "Dr. Sinda Wyman", "1-270-665-9933x1626", "http://www.paucek.com/search.htm"},
	})
	if err != nil {
		fmt.Println(err.Error())
	}

	sNstring := string(sortName) //sorted name string
	//fmt.Println(sNstring)
	eNString := string(expectedNameR) //expected sorted name string
	//fmt.Println(eNString)

	if sNstring != eNString {
		t.Fatalf("Failed to sort by Name returned with wrong order result %s \n", failed)
	} else {
		t.Logf("Success sorting by Name returned with expected order of result %s \n", succeed)
	}

	//TEST SORTH BY RATINGS
	sortRatings, err := json.Marshal(SortByRating(hotelx)) //SortByRating
	if err != nil {
		fmt.Println(err.Error())
	}

	expectedRatingsR, err := json.Marshal([]Hotel{
		{"Apartment Dörr Bolzmannweg", "451, 05116 Hannover", 1, "Scarlet Kusch-Linke", "08177354570", "http://www.garden.com/list/home.html"},
		{"Bpartment Dörr Bolzmannweg", "451, 05116 Hannover", 2, "Scarlet Kusch-Linke", "08177354570", "http://www.garden.com/list/home.html"},
		{"The Gibson", "63847 Lowe Knoll, East Maxine, WA 97030-4876", 4, "Dr. Sinda Wyman", "1-270-665-9933x1626", "http://www.paucek.com/search.htm"},
		{"One Gibson", "63847 Lowe Knoll, East Maxine, WA 97030-4876", 5, "Dr. Sinda Wyman", "1-270-665-9933x1626", "http://www.paucek.com/search.htm"},
	})
	if err != nil {
		fmt.Println(err.Error())
	}

	sRString := string(sortRatings)      //sorted rating string
	eRString := string(expectedRatingsR) //expected sorted ratings string
	if sRString != eRString {
		t.Fatalf("Failed to sort by Ratings returned with wrong order result %s \n", failed)
	} else {
		t.Logf("Success sorting by Ratings returned with expected order of result %s \n", succeed)
	}
}
