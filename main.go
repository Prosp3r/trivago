package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
	//"github.com/go-yaml/yaml"
)

//Convertfile ...interface to be implemented by all formats to be used
type Convertfile interface {
	Convert()
	Type()
}

//Hotel struct holds hotels structure for use by other dependent modules
type Hotel struct {
	Name    string
	Address string
	Stars   int
	Contact string
	Phone   string
	URI     string
}

//::::::::::::::::::::::::::END OF IMPLEMENTATIONS::::::::::::::::::::::::::::::
var hotels []Hotel
var invhotels []Hotel

//SortNames ...
var SortNames bool

//SortRatings ...
var SortRatings bool

//FileSrc ...source for file to load
var FileSrc string //= "hotel.csv"

//var defaultFormats YAML //JSON
var yamlFormats YAML //YAML
var jsonFormats JSON //JSON
//var YourExtendableFormatHere JSON

var readStats bool

//ValidEntry holds the count for valid hotel entries
var ValidEntry int

//InvalidEntry holds the count for invalid hotel entries
var InvalidEntry int

//OpsStats ... reads in stats for the operations
var OpsStats string

//OrderByName ... orders hotels by name if set to true using flags
var OrderByName bool

//OrderByRatings ..orders hotels by ratings if set to true using flags
var OrderByRatings bool

func main() {

	//Run OS flags
	flagger()

	//READ FILE FROM SOURCE
	f, err := readFile(FileSrc)
	if err != nil {
		fmt.Printf("Cannot read file: %v following error occured: %v \n", FileSrc, err.Error())
	}

	//::::::::::::EXTENDABLE SECTION:::::::::::::::::::::::
	//CONVERT TO DESIRED FORMAT
	jasonOutput, err := jsonFormats.Convert(f)
	if err != nil {
		fmt.Printf("An error was encountered: %v \n", err.Error())
	}
	//fmt.Println(f)

	yamlOutput, err := yamlFormats.Convert(f)
	if err != nil {
		fmt.Printf("An error was encountered: %v \n", err.Error())
	}
	/*
		For new format implementation
		yamlOutput, err := yamlFormats.Convert(f)
		if err != nil {
			fmt.Printf("An error was encountered: %v \n", err.Error())
		}
	*/

	//CREATE A FILE NAME FROM RANDOME CHARACTERS
	yamlExt := string(yamlFormats.Type())
	jsonExt := string(jsonFormats.Type())
	/*ADD NEW FORMAT IMPLEMENTATION HERE*/
	/*jsonExt := string(jsonFormats.Type())*/

	jsonName := "./converted/" + GetRandomChar(6) + "." + jsonExt
	yamlName := "./converted/" + GetRandomChar(6) + "." + yamlExt
	/*ADD NEW FORMAT IMPLEMENTATION HERE*/
	/*yamlName := "./converted/" + getRandomChar(6) + "." + yamlExt*/

	_ = saveFile(yamlName, yamlOutput)  //saveYAML :
	_ = saveFile(jsonName, jasonOutput) //saveJSON :
	/*ADD NEW FORMAT IMPLEMENTATION HERE*/
	/*_ = saveFile(jsonName, jasonOutput) //saveJSON :*/

	//::::::::::::END EXTENDABLE SECTION:::::::::::::::::::::::

	//_, err = os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, 0644)
	//err = ioutil.WriteFile(filename, finalOutput, 0644)
	//fmt.Printf("File saved to %v \n", filename)

	//fmt.Printf("Stats: %v \n", stats)
	//STATISTICS
	if readStats {
		OpsStats = "Valid entries : " + strconv.Itoa(ValidEntry) + "   Total Invalid Entries : " + strconv.Itoa(InvalidEntry)
		fmt.Printf("Stats: %v \n", OpsStats)
	}
}

func saveFile(filename string, output []byte) bool {
	//finalOutput, err := endFormats.Convert(f)
	_, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Printf("An error was encountered: %v \n", err.Error())
		return false
	}
	err = ioutil.WriteFile(filename, output, 0644)
	fmt.Printf("File saved to %v \n", filename)
	return true
}

//READER
//readFile will read the given .csv file into custom struct type in memory
func readFile(fileSource string) ([]Hotel, error) {
	dataFile, _ := os.Open(fileSource)
	reader := csv.NewReader(bufio.NewReader(dataFile))

	for {
		line, err := reader.Read()
		//check for end of file
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Printf("Cannot read file: %v following error occured: %v \n", fileSource, err.Error())
		}

		rating, err := strconv.Atoi(line[2]) //(line[2])
		if err != nil {
			//rating conversion failed
		}
		//rating := ParseInt(lin[2])

		if IsValidHotel(line) == true {
			hotels = append(hotels, Hotel{
				Name:    line[0],
				Address: line[1],
				Stars:   rating,
				Contact: line[3],
				Phone:   line[4],
				URI:     line[5],
			})
			ValidEntry++

		} else {
			//count invalid hotels listings for stats and reporting
			invhotels = append(hotels, Hotel{
				Name:    line[0],
				Address: line[1],
				Stars:   rating,
				Contact: line[3],
				Phone:   line[4],
				URI:     line[5],
			})
			InvalidEntry++
		}
	}

	//SORTING
	if OrderByName == true {
		hotels = SortByName(hotels)
	}

	if OrderByRatings == true {
		hotels = SortByRating(hotels)
	}
	//END SORTING
	//fmt.Println(hotels)
	//os.Exit(1)
	return hotels, nil
}

//:::::::::::::::UTILITY CODE::::::::::::::::::::::::::::::::

//GetRandomChar ... returns random character of given length
func GetRandomChar(maxChar int) string {

	//seed random generator
	rand.Seed(time.Now().UnixNano())

	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"abcdefghijklmnopqrstuvwxyz" +
		"0123456789")
	length := maxChar //8
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}
	str := b.String() // E.g. "ExcbsVQs"
	return str
}

//:::::::::::::::END UTILITY CODE::::::::::::::::::::::::::::::::

/*func processCSV(rc io.Reader) (ch chan []string) {
	ch = make(chan []string, 10)
	go func() {
		r := csv.NewReader(rc)
		if _, err := r.Read(); err != nil { //read header
			log.Fatal(err)
		}
		defer close(ch)
		for {
			rec, err := r.Read()
			if err != nil {
				if err == io.EOF {
					break
				}
				log.Fatal(err)

			}
			ch <- rec
		}
	}()
	return
}
*/
//fmt.Println("Hello Trivago")
//pick flags
//get source file
//get destination
//get sorting/group
//get format [json, yaml, ]
//get stats...statistics report after parsing(error count, success count, total records, etc)
//flag.Parse()
//if
