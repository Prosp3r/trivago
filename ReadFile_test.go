package main

import (
	"fmt"
	"testing"
)

func TestReadFile(t *testing.T) {
	fileSrc := "hotel.csv"
	//READ FILE FROM SOURCE
	h, err := readFile(fileSrc)
	if err != nil {
		fmt.Printf("Cannot read file: %v following error occured: %v \n", FileSrc, err.Error())
	}
	hcount := 0
	for _, v := range h {
		if len(v.Name) > 2 {
			hcount++ //hotel counts
		}
	}
	if hcount < 2 {
		t.Fatalf("Failed readFile returned with too little result %s \n", failed)
	} else {
		t.Logf("Success readFile returned with minimum expected result %s \n", succeed)
	}
}
