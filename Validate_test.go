package main

import (
	"fmt"
	"testing"
)

func TestValidate(t *testing.T) {

	//testing hotel validation
	//line1 := []string{Schlosser Apartment Jürgen-Gehringer-Ring 99, 55927 Düren 2 Philine Textor-Haering (04115) 30514 http://www.hecker.com/}
	line1 := []string{"The Gibson", "63847 Lowe Knoll, East Maxine, WA 97030-4876", "5", "Dr. Sinda Wyman", "1-270-665-9933x1626", "http://www.paucek.com/search.htm"}
	line2 := []string{"The Gibson", "63847 Lowe Knoll, East Maxine, WA 97030-4876", "15", "Dr. Sinda Wyman", "1-270-665-9933x1626", "http://www.paucek.com/search.htm"}
	line3 := []string{"The Gibson", "63847 Lowe Knoll, East Maxine, WA 97030-4876", "2", "Dr. Sinda Wyman", "1-270-665-9933x1626", "http:www.paucek.com/search.htm"}
	line4 := []string{"The Gibson", "63847 Lowe Knoll, East Maxine, WA 97030-4876", "-2", "Dr. Sinda Wyman", "1-270-665-9933x1626", "http://www.paucek.com/search.htm"}
	line5 := []string{"The a\xc5z", "63847 Lowe Knoll, East Maxine, WA 97030-4876", "2", "Dr. Sinda Wyman", "1-270-665-9933x1626", "http://www.paucek.com/search.htm"}
	//line5 := []string{"The Gibso��$", "63847 Lowe Knoll, East Maxine, WA 97030-4876", "2", "Dr. Sinda Wyman", "1-270-665-9933x1626", "http://www.paucek.com/search.htm"}
	//line := []string{"Schlosser a\xc5z Apartment Jürgen-Gehringer-Ring 99, 55927 Düren 2 Philine Textor-Haering (04115) 30514 http://www.hecker.com/"}

	vhTrue := IsValidHotel(line1)
	vhFalseOne := IsValidHotel(line2)
	vhFalseTwo := IsValidHotel(line3)
	vhFalseThree := IsValidHotel(line4)
	vhFalseFour := IsValidHotel(line5)

	fmt.Println(vhTrue)
	fmt.Println(vhFalseOne)
	fmt.Println(vhFalseTwo)

	if vhTrue == false {
		t.Logf("Failed Proper entry validation returned with incorrect validation response %s \n", failed)
	} else {
		t.Logf("Success Proper entry validation returned with correct validation response %s \n", succeed)
	}

	if vhFalseOne == true {
		t.Logf("Failed improper(RATING) entry validation returned with incorrect validation response %s \n", failed)
	} else {
		t.Logf("Success improper(RATING) entry validation caught error in validation response %s \n", succeed)
	}

	if vhFalseTwo == true {
		t.Fatalf("Failed improper(URI) entry validation returned with incorrect validation response %s \n", failed)
	} else {
		t.Logf("Success improper(URI) entry validation caught error in validation response %s \n", succeed)
	}

	if vhFalseThree == true {
		t.Fatalf("Failed improper(NEGATIVE RATING) entry validation returned with incorrect validation response %s \n", failed)
	} else {
		t.Logf("Success improper(NEGATIVE RATING) entry validation caught error in validation response %s \n", succeed)
	}

	if vhFalseFour == true {
		t.Fatalf("Failed improper(NON-UTF8) entry validation returned with incorrect validation response %s \n", failed)
	} else {
		t.Logf("Success improper(NON-UTF8) entry validation caught error in validation response %s \n", succeed)
	}

}
