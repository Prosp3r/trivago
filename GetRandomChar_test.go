package main

import (
	"testing"
)

const (
	succeed = "\u2713"
	failed  = "\u2717"
)

func TestGetRandomChar(t *testing.T) {
	limits := []struct {
		x int
	}{
		{6},
		{8},
		{10},
		{7},
		{0},
		{1},
	}
	for _, limit := range limits {
		rchar := GetRandomChar(limit.x)
		if len(rchar) != limit.x {
			t.Fatalf("Failed getRandomChar returned with inappropriate character count %s \n", failed)
		} else {
			t.Logf("Success getRandomChar returned with inappropriate character count %s \n", succeed)
		}
	}

	//rchar := GetRandomChar(limit)

}
