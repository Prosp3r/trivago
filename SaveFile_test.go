package main

import "testing"

//TestSaveFile ...
func TestSaveFile(t *testing.T) {
	//sample data
	yamlOutput := []byte{45, 32, 110, 97, 109, 101, 58, 32, 84, 104, 101, 32, 71, 105, 98, 115, 111, 110, 10, 32, 32, 97, 100, 100, 114, 101, 115, 115, 58, 32, 54, 51, 56, 52, 55, 32, 76, 111, 119, 101, 32, 75, 110, 111, 108, 108, 44, 32, 69, 97, 115, 116, 32, 77, 97, 120, 105, 110, 101, 44, 32, 87, 65, 32, 57, 55, 48, 51, 48, 45, 52, 56, 55, 54, 10, 32, 32, 115, 116, 97, 114, 115, 58, 32, 53, 10, 32, 32, 99, 111, 110, 116, 97, 99, 116, 58, 32, 68, 114, 46, 32, 83, 105, 110, 100, 97, 32, 87, 121, 109, 97, 110, 10, 32, 32, 112, 104, 111, 110, 101, 58, 32, 49, 45, 50, 55, 48, 45, 54, 54, 53, 45, 57, 57, 51, 51, 120, 49, 54, 50, 54, 10, 32, 32, 117, 114, 105, 58, 32, 104, 116, 116, 112, 58, 47, 47, 119, 119, 119, 46, 112, 97, 117, 99, 101, 107, 46, 99, 111, 109, 47, 115, 101, 97, 114, 99, 104, 46, 104, 116, 109, 10}

	samplefileName := "./converted/" + "sample_file.yml"

	saver := saveFile(samplefileName, yamlOutput)
	if saver == true {
		t.Logf("Success! saveFile returned with correct validation response %s \n", succeed)
	} else {
		t.Logf("Failed! saveFile returned with incorrect validation response %s \n", failed)
	}

	fakesamplefileName := "./converted/" + "."
	fakeSaver := saveFile(fakesamplefileName, yamlOutput)
	if fakeSaver == false {
		t.Logf("Success! saveFile returned with correct validation response for fake content name %s \n", succeed)
	} else {
		t.Logf("Failed! saveFile returned with incorrect validation response for fake content name %s \n", failed)
	}
}
